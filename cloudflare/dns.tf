locals {
  direct_CNAME = {
    "_acme-challenge.auth" : "auth.acme.c2games.org",
    "_acme-challenge.sandbox" : "sandbox.acme.c2games.org",
    "_acme-challenge.yopass" : "yopass.acme.c2games.org",
    "c2" : "c2.bdavis.me",
    "sandbox" : "sandbox3.ncaecybergames.org",
    "mail" : "ghs.googlehosted.com",
    "calendar" : "ghs.googlehosted.com",
    "cny2023" : "maltiverse.ovh.malteksolutions.com",
    "argocd" : "k8s.ovh.ncaecybergames.org",
    "storage.vce1" : "vce1-storage-r5m4n.ondigitalocean.app",
    "storage.vce2" : "vce2-storage-iuwmx.ondigitalocean.app"
  }
  proxy_CNAME = {
    "auth" : "a286df5e-c894-474c-b5c4-eb21d85211b0.cfargotunnel.com",
    "api" : "k8s.ovh.ncaecybergames.org",
    "api.dev" : "k8s.ovh.ncaecybergames.org",
  }
  direct_A = {
    "*.local" : "127.0.0.1"
    "bot" : "45.55.120.218",
    "bw" : "167.99.21.237",
    "ca" : "172.18.0.38",
    "cdn" : "172.18.12.25",
    "dev.red" : "145.40.88.217",
    "k3s-controlplane" : "172.18.118.200",
    "k3s-controlplane01" : "172.18.118.201",
    "k3s-controlplane02" : "172.18.118.202",
    "k3s-controlplane03" : "172.18.118.203",
    "k3s-worker01" : "172.18.118.211",
    "k3s-worker02" : "172.18.118.212",
    "k3s-worker03" : "172.18.118.213",
    "k3s-worker04" : "172.18.118.214",
    "k3s-worker05" : "172.18.118.215",
    "k3s-worker06" : "172.18.118.216",
    "k8s.ovh" : "40.160.6.24",
    "sandbox1" : "150.154.1.88",
    "sandbox2" : "150.154.1.145",
    "sandbox3" : "150.154.1.153",
    "sandbox4" : "150.154.1.154",
    "vce6" : "136.144.51.23",
    "*.localhost" : "127.0.0.1",
    "localhost" : "127.0.0.1",
    "ssl-test" : "40.160.6.24",
    "pmp" : "167.99.21.237",
    "pmp2" : "143.198.13.68",
    "conf" : "167.99.21.237",
    "edge.portainer" : "159.203.143.98",
    "traefik" : "104.131.171.109",
    "unifi" : "157.245.217.255",
    "yopass" : "45.55.120.218"
  }
  proxied_A = {
  }
  TXT = {
    "_dmarc" : "\"v=DMARC1; p=none; rua=mailto:postmaster@c2games.org; adkim=r\"",
    "google._domainkey" : "\"v=DKIM1; k=rsa; p=MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAwRfBJwOjz+rEqr0qaJHFfofQOkcVY1AsMjfFHpEQjMcmN/F7O5ny53wqayCC5aLD4a4GSTvF96/KgR5XqqXTD08TBZTf8Pf1pyuORftZ0XsOt/dWd9o9BuMWgfAOwT4KBTIL35102i71Un9v33CesMLM8QBg+92QLsl3OuvG96xfxgnXpy9YOJiIE0efkhznW VsgVSej3ZJxuVFCUOrGGhTays9zal1oy7RX1QcxROrtJ5HIPpAVgxGVINFCLVMC7VlFlzvfSxilN4dj1FIKWfqWlzhqq1TiBdnBHv9iK8jTyDyPPxV/Kpn2JhyBmXPJl06nj5ad3NhGtRX0GwBKxQIDAQAB\"",
    "cm._domainkey" : "\"k=rsa; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDX9nfWVZyhoepeQZdhsHlROs0oKvCqAMzdEGadAEOiDRIVFHc2VPAbVsCoZCfUnb3jPGcaod6WmLUQPbAQN7SZeOejIWlXmXWYxn3xNG8MtRVv3i57vxLFGt7iBu7wXZ+BUEDBzPnzH5q7PIG9tjIzooAK/HB8HneOL9a//pi5AQIDAQAB\"",
    "@" : "\"google-site-verification=D9qhuV5BWqKAAxh_6_OWlIMOMScj6Egnd0uOYnmWYvw\"",
  }
  vce_NS = {
    "@" : "pns101.cloudns.net",
    "@" : "pns102.cloudns.net",
  }
}

#
# Unproxied CNAME records
#
resource "cloudflare_dns_record" "direct_CNAME" {
  for_each = local.direct_CNAME
  zone_id  = data.cloudflare_zone.ncaecybergames_org.zone_id
  name     = each.key
  content  = each.value
  type     = "CNAME"
  proxied  = false
  ttl      = 1
  tags     = ["iac:gitlab"]
}

#
# Proxied CNAME records
#
resource "cloudflare_dns_record" "proxy_CNAME" {
  for_each = local.proxy_CNAME
  zone_id  = data.cloudflare_zone.ncaecybergames_org.zone_id
  name     = each.key
  content  = each.value
  type     = "CNAME"
  proxied  = true
  ttl      = 1
  tags     = ["iac:gitlab"]
}

#
# Unproxied A records
#
resource "cloudflare_dns_record" "direct_A" {
  for_each = local.direct_A
  zone_id  = data.cloudflare_zone.ncaecybergames_org.zone_id
  name     = each.key
  content  = each.value
  type     = "A"
  proxied  = false
  ttl      = 1
  tags     = ["iac:gitlab"]
}

#
# Proxied A records
#
resource "cloudflare_dns_record" "proxied_A" {
  for_each = local.proxied_A
  zone_id  = data.cloudflare_zone.ncaecybergames_org.zone_id
  name     = each.key
  content  = each.value
  type     = "A"
  proxied  = true
  ttl      = 1
  tags     = ["iac:gitlab"]
}

#
# Keep this its own loop for cleanliness
#
resource "cloudflare_dns_record" "teamrouter" {
  count   = 26
  zone_id = data.cloudflare_zone.ncaecybergames_org.zone_id
  name    = "team${count.index}"
  content = "172.18.13.${count.index}"
  type    = "A"
  proxied = false
  ttl     = 1
  tags    = ["iac:gitlab"]
}

resource "cloudflare_dns_record" "teamweb" {
  count   = 26
  zone_id = data.cloudflare_zone.ncaecybergames_org.zone_id
  name    = "www.team${count.index}"
  content = "172.18.13.${count.index}"
  type    = "A"
  proxied = false
  ttl     = 1
  tags    = ["iac:gitlab"]
}

resource "cloudflare_dns_record" "sandbox_acme" {
  count   = 6
  zone_id = data.cloudflare_zone.ncaecybergames_org.zone_id
  name    = "_acme-challenge.sandbox${count.index}"
  content = "_acme-challenge.sandbox.acme.c2games.org"
  type    = "CNAME"
  proxied = false
  ttl     = 1
  tags    = ["iac:gitlab"]
}

resource "cloudflare_dns_record" "vce_acme" {
  count   = 6
  zone_id = data.cloudflare_zone.ncaecybergames_org.zone_id
  name    = "vce${count.index}"
  content = "${count.index}.vce.ncaecybergames.org"
  type    = "CNAME"
  proxied = false
  ttl     = 1
  tags    = ["iac:gitlab"]
}

#
# TXT Records
#
resource "cloudflare_dns_record" "TXT" {
  for_each = local.TXT
  zone_id  = data.cloudflare_zone.ncaecybergames_org.zone_id
  name     = each.key
  content  = each.value
  type     = "TXT"
  ttl      = 1
  tags     = ["iac:gitlab"]
}

#
# NS Records
#
resource "cloudflare_dns_record" "NS" {
  for_each = local.vce_NS
  zone_id  = data.cloudflare_zone.ncaecybergames_org.zone_id
  name     = "vce"
  content  = each.value
  type     = "NS"
  ttl      = 1
  tags     = ["iac:gitlab"]
}

#
# MX Records
#
resource "cloudflare_dns_record" "MX" {
  zone_id  = data.cloudflare_zone.ncaecybergames_org.zone_id
  name     = "@"
  content  = "smtp.google.com"
  type     = "MX"
  ttl      = 1
  tags     = ["iac:gitlab"]
  priority = 1
}

#
# Keep these separate due to duplicate name
#
resource "cloudflare_dns_record" "dmarc1" {
  zone_id = data.cloudflare_zone.ncaecybergames_org.zone_id
  name    = "@"
  content = "v=DMARC1; p=none; rua=mailto:postmaster@c2games.org"
  type    = "TXT"
  ttl     = 1
  tags    = ["iac:gitlab"]
}

resource "cloudflare_dns_record" "spf" {
  zone_id = data.cloudflare_zone.ncaecybergames_org.zone_id
  name    = "@"
  content = "v=spf1 include:_spf.google.com include:_spf.createsend.com ~all"
  type    = "TXT"
  ttl     = 1
  tags    = ["iac:gitlab"]
}
