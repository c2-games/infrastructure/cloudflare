locals {
  # URLs that Cloudflare should protect using ZeroTrust Access
  url_access_control = [
    {
      name : "Internal Docs",
      domains : [
        # generated pages.dev domains
        "*.internal-documentation-ah2.pages.dev",
        "internal-documentation-ah2.pages.dev",
        # custom exposed domain
        "*.internal.docs.ncaecybergames.org",
        "internal.docs.ncaecybergames.org"
      ],
      groups : ["staff"],
    },
    # limit red-team and black-team doc access
    # we don't restrict dev branches or pages.dev domains, because if you know about those,
    # you can probably just access the repo anyways
    {
      name : "Redteam Internal Docs",
      domains : ["internal.docs.ncaecybergames.org/red-team"],
      groups : ["red", "red-admin", "black-admin", "scoring"]
    },
    {
      name : "Blackteam Internal Docs",
      domains : ["internal.docs.ncaecybergames.org/black-team"],
      groups : ["black", "black-admin", "red-admin"],
    },
    # legacy pointer to previously used domain
    {
      name : "(Legacy) Redteam Docs",
      domains : ["docs.red.ncaecybergames.org"],
      groups : ["staff"]
    },
    {
      name : "www3",
      domains : ["www3.ncaecybergames.org"],
      groups : ["staff"]
    },
    {
      name : "Graylog",
      domains : ["graylog.ncaecybergames.org"],
      groups : ["staff"]
    },
    {
      name : "Dell OME",
      domains : ["ome.ncaecybergames.org"],
      groups : ["staff"]
    },
    {
      name : "Primary UI Dev Pages",
      domains : ["*.primary-ui.pages.dev"],
      groups : ["staff"]
    },
    {
      name : "WWW2 Dev Pages",
      domains : ["*.www2-f37.pages.dev"],
      groups : ["staff"]
    },
    {
      name : "Hasura Console",
      domains : ["api.ncaecybergames.org/console", "api.dev.ncaecybergames.org/console"],
      groups : ["staff"]
    },
    {
      name : "ArgoCD",
      domains : ["argocd.ncaecybergames.org"],
      groups : ["scoring"]
    },
    {
      name : "Grafana VCE1",
      domains : ["monitoring.k3s.vce1.ncaecybergames.org"],
      groups : ["scoring"]
    },
    {
      name : "Longhorn VCE1",
      domains : ["longhorn.k3s.vce1.ncaecybergames.org"],
      groups : ["scoring"]
    },
    {
      name : "Sandbox4-K3s Monitoring",
      domains : ["prometheus2.k3s.sandbox4.ncaecybergames.org"],
      groups : ["scoring"]
    }
  ]

  # These are just shortcuts
  bookmarks = {
    "Keycloak Dev" : "auth.ncaecybergames.org/admin/dev/console",
    "Keycloak Prod" : "auth.ncaecybergames.org/admin/prod/console",
    "Test Authentication" : "c2games.cloudflareaccess.com/cdn-cgi/access/test-idp/8ea62ce2-c6dd-4c3a-a822-1fb169346805",
    "GitLab" : "gitlab.com/c2-games",
    "VaultWarden" : "bw.c2games.org",
  }

  # Convert saml groups to Cloudflare Access groups
  saml_groups = {
    # saml_group: "CF Group Name"
    "staff" : {
      "group" : "/staff",
      "name" : "Staff",
      "role" : "staff"
    },
    "scoring" : {
      "group" : "/staff/scoring",
      "name" : "Scoring",
      "role" : "scoring"
    },
    "red" : {
      "group" : "/staff/red",
      "name" : "Redteam",
      "role" : "red"
    },
    "red-admin" : {
      "group" : "/staff/red-admin",
      "name" : "Redteam Admin",
      "role" : "red_admin"
    },
    "black" : {
      "group" : "/staff/black",
      "name" : "Blackteam",
      "role" : "black"
    },
    "black-admin" : {
      "group" : "/staff/black-admin",
      "name" : "Blackteam Admin",
      "role" : "black_admin"
    },
    "event-admin" : {
      "group" : "/staff/event-admin",
      "name" : "Event Admin",
      "role" : "event_admin"
    }
  }

  # create map from list
  url_access_control_map = {
    for item in local.url_access_control : item.name => item
  }
}

resource "cloudflare_zero_trust_access_group" "saml_groups" {
  for_each   = local.saml_groups
  account_id = "d8b233f31269b6afe38499cbfdf66851"
  name       = each.value["name"]

  include = [{
    saml = {
      attribute_name       = "groups"
      attribute_value      = each.value["group"]
      identity_provider_id = "8ea62ce2-c6dd-4c3a-a822-1fb169346805"
    }
  }]
}

resource "cloudflare_zero_trust_access_policy" "saml_groups" {
  for_each   = local.saml_groups
  account_id = "d8b233f31269b6afe38499cbfdf66851"
  name       = each.key
  decision   = "allow"

  include = [{
    group = {
      id = cloudflare_zero_trust_access_group.saml_groups[each.key].id
    }
  }]
}

resource "cloudflare_zero_trust_access_application" "url_access_controls" {
  for_each = local.url_access_control_map

  account_id                 = "d8b233f31269b6afe38499cbfdf66851"
  type                       = "self_hosted"
  name                       = each.value.name
  self_hosted_domains        = each.value.domains
  destinations               = [for domain in each.value.domains : { type = "public", uri = domain }]
  allowed_idps               = ["8ea62ce2-c6dd-4c3a-a822-1fb169346805"]
  auto_redirect_to_identity  = true
  http_only_cookie_attribute = true
  policies = [for group in each.value.groups :
    { decision = "allow", id = cloudflare_zero_trust_access_policy.saml_groups[group].id }
  ]
  tags = ["iac:gitlab"]
}

resource "cloudflare_zero_trust_access_application" "bookmarks" {
  for_each   = local.bookmarks
  account_id = "d8b233f31269b6afe38499cbfdf66851"
  name       = each.key
  domain     = each.value
  type       = "bookmark"
  tags       = ["iac:gitlab"]
}
