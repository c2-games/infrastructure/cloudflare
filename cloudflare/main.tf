terraform {
  required_providers {
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = ">=5.1.0"
    }
  }
}


data "cloudflare_zone" "ncaecybergames_org" {
  zone_id = "b94c0908d8643ada802b980498173b5e"
}

data "cloudflare_zero_trust_access_identity_provider" "saml" {
  account_id           = "d8b233f31269b6afe38499cbfdf66851"
  identity_provider_id = "8ea62ce2-c6dd-4c3a-a822-1fb169346805"
}
