[[_TOC_]]

## Adding new DNS records

### Basic records

This is a fairly straightforward process for basic records, simply add a new cloudflare_record instance in [dns.tf](dns.tf) and commit:

```terraform
resource "cloudflare_record" "localhost" {
  zone_id  = data.cloudflare_zone.ncaecybergames_org.zone_id
  name     = "localhost"
  value    = "127.0.0.1"
  type     = "A"
}
```

### Special case: Complex record types

More complex records like SRV require a slightly different config:

```terraform
resource "cloudflare_record" "_minecraft_tcp" {
  zone_id = data.cloudflare_zone.ncaecybergames_org.zone_id
  name    = "_minecraft_tcp"
  type    = "SRV"

  data {
    service  = "_minecraft"
    proto    = "_tcp"
    name     = "vanilla"
    priority = 0
    weight   = 0
    port     = 25565
    target   = "vanilla.ncaecybergames.org"
  }
}
```

### Special case: ACME

Shared ACME records with c2games.org have a special case to simplify the config. Add the new domain to the [acme_domains set](dns.tf#L2), and a CNAME will be automatically created with the appropriate values:


```terraform
locals {
  acme_domains = toset([
    "existing",
    "<yourthinghere>",
  ])
}
```

## Applying changes

Gitlab CI will automatically create and run a pipeline for commits to main which will initialize Terraform and plan any changes. Assuming that succeeds, any maintainer can run the final deploy job manually. As a shortcut, here's a [link to the latest pipeline](https://gitlab.com/c2-games/infrastructure/cloudflare/-/pipelines/latest).
