#!/bin/bash
set -e

source .env
tofu init \
    -backend-config="address=https://gitlab.com/api/v4/projects/32534045/terraform/state/production" \
    -backend-config="lock_address=https://gitlab.com/api/v4/projects/32534045/terraform/state/production/lock" \
    -backend-config="unlock_address=https://gitlab.com/api/v4/projects/32534045/terraform/state/production/lock" \
    -backend-config="username=$GITLAB_USERNAME" \
    -backend-config="password=$GITLAB_ACCESS_TOKEN" \
    -backend-config="lock_method=POST" \
    -backend-config="unlock_method=DELETE" \
    -backend-config="retry_wait_min=5" \
    -upgrade