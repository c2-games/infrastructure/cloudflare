terraform {
  required_providers {
    keycloak = {
      source  = "mrparkers/keycloak"
      version = ">= 4.3.1"
    }
    http = {
      source  = "hashicorp/http"
      version = ">=3.4.0"
    }
  }
}

data "keycloak_realm" "realm" {
  realm = "prod"
}

/*
data "http" "certs" {
  url = "https://auth.c2games.org/realms/prod/protocol/openid-connect/certs"

  request_headers = {
    Accept = "application/json"
  }

  lifecycle {
    postcondition {
      condition     = contains([200, 201, 204], self.status_code)
      error_message = "Status code invalid"
    }
  }
}
*/

resource "keycloak_saml_client" "cloudflare" {
  realm_id                   = data.keycloak_realm.realm.id
  client_id                  = "https://c2games.cloudflareaccess.com/cdn-cgi/access/callback"
  signature_key_name         = "NONE"
  signature_algorithm        = "RSA_SHA256"
  client_signature_required  = false
  master_saml_processing_url = "https://auth.c2games.org/realms/prod/protocol/saml"
  valid_redirect_uris = [
    "https://c2games.cloudflareaccess.com/cdn-cgi/access/callback"
  ]
}


data "keycloak_realm_keys" "realm_keys" {
  realm_id   = data.keycloak_realm.realm.id
  algorithms = [data.keycloak_realm.realm.default_signature_algorithm]
  status     = ["ACTIVE"]
}
