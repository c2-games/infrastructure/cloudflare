terraform {
  required_providers {
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = ">=5.1.0"
    }
    keycloak = {
      source  = "mrparkers/keycloak"
      version = ">= 4.3.1"
    }
    googleworkspace = {
      source  = "hashicorp/googleworkspace"
      version = "0.7.0"
    }
    http = {
      source  = "hashicorp/http"
      version = ">=3.4.0"
    }
  }

  backend "http" {
  }

  encryption {
    state {
      enforced = true
    }
    plan {
      enforced = true
    }
  }
}

provider "keycloak" {
  url       = "https://auth.ncaecybergames.org"
  client_id = "gitlab-terraform"
}

module "cloudflare" {
  source = "./cloudflare"
}

module "keycloak-dev" {
  source = "./keycloak-dev"
}

module "keycloak-prod" {
  source = "./keycloak-prod"
}

module "googleworkspace" {
  source = "./googleworkspace"
}
