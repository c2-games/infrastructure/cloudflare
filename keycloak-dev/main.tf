terraform {
  required_providers {
    keycloak = {
      source  = "mrparkers/keycloak"
      version = ">= 4.3.1"
    }
  }
}

data "keycloak_realm" "realm" {
  realm = "dev"
}
